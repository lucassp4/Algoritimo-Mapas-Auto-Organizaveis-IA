package com.unialfa.ia.model;

import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ModelGenerator {

    //Organizando o codigo para ficar mais legivél resolvi separar o carregamento da base em uma classe separada...

    public Instances loadDataset(String path) {
        Instances dataset = null;
        try {
            dataset = ConverterUtils.DataSource.read(path);
            if (dataset.classIndex() == -1) {
                dataset.setClassIndex(dataset.numAttributes() - 1);
            }
        } catch (Exception ex) {
            Logger.getLogger(ModelGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }

        return dataset;
    }
}
