package com.unialfa.ia;

import com.unialfa.ia.SOM.SelfOrganizingMap;
import com.unialfa.ia.model.ModelGenerator;
import weka.classifiers.evaluation.Evaluation;
import weka.clusterers.ClusterEvaluation;
import weka.core.Debug;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.pmml.jaxbbindings.Cluster;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Normalize;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.Normalizer;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Main {
    //@param filename      O nome do Arquivo ARFF que será utilizado com base
    public static final String DATASETPATH = "C:\\Users\\lucas.pereira\\IdeaProjects\\weka-api\\src\\com\\unialfa\\ia\\DataSource\\Autism-Child-Data.arff";
    public static final String MODElPATH = "C:\\Users\\lucas.pereira\\IdeaProjects\\weka-api\\src\\com\\unialfa\\ia\\DataSource\\New-Autism-Child-Data.arff";


    public static void main(String[] args) throws Exception {

        //Inicializa objeto Modelo de Carregamento da Base
        ModelGenerator mg = new ModelGenerator();

        //Instancia a base de dados
        Instances dataSet =  mg.loadDataset(DATASETPATH);

        //Vamos dividir a base de dados para ser treinada em 80% e 20% para testes
        int trainSize = (int) Math.round(dataSet.numInstances()* 0.8);
        int testSize = dataSet.numInstances() - trainSize;

        //Normalizando os dados...
        dataSet.randomize(new Debug.Random(1)); // Se comentar está linha a precisão do modelo será reduzida...

        //Filtro para remover o indice a ser analisado..
        weka.filters.unsupervised.attribute.Remove filter = new weka.filters.unsupervised.attribute.Remove();
        filter.setAttributeIndices(" "+(dataSet.classIndex() + 1));
        try {
            filter.setInputFormat(dataSet);
            Instances dataClusterer = Filter.useFilter(dataSet, filter);
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        //Metodo apenas para saida do dados e saber se está OK o carregamento
        System.out.println(dataSet.toSummaryString());

        Instances traindataset = new Instances(dataSet, 0, trainSize);
        Instances testdataset = new Instances(dataSet, trainSize, testSize);

        //Inicializamos um objeto da classe SOM
        SelfOrganizingMap SOM = new SelfOrganizingMap();

        //Normalização de Atributos
        SOM.setNormalizeAttributes(true);

        //Setandondo o Parametro de Opção a ser trabalhada... //Inicializando Aprendizagem...
        String[] options = new String[0];
        options[0] = "L";

        //Setando a opção escolhida
        SOM.setOptions(options);

        //inicializamos o carregamento da instancia e contruimos o Cluster...
        SOM.buildClusterer(traindataset);

        //Parametro que informa se será calculado estatisticamente...
        SOM.setCalcStats(true);

        //setando a convergencia de qual o limite de epocas será executado...
        SOM.setConvergenceEpochs(1000);

        //Não executaremos em modo debug
        SOM.setDebug(false);

        //Checar as compatibilidades entre clusters...
        SOM.setDoNotCheckCapabilities(false);

        //Altura da Matriz
        SOM.setHeight(2);

        //Largura da Matriz
        SOM.setWidth(2);

        // Taxa de Aprendizado
        SOM.setLearningRate(1.0);

        //Ordenar as Epocas, que são realizadas a cada interação de Processo Cooperativo e do processo adaptativo
        SOM.setOrderingEpochs(2000);

        //Construindo a cluesterização...
        SOM.buildClusterer(traindataset);

/*

        //Adicionando um novo registro na instancia porém isso não quer dizer que ela esteja nos registros da base.. Apenas para teste de predição
        Instance ins = new DenseInstance(20);
        ins.setValue(0, "1");
        ins.setValue(1, "0");
        ins.setValue(2, "1");
        ins.setValue(3, "1");
        ins.setValue(4, "1");
        ins.setValue(5, "1");
        ins.setValue(6, "0");
        ins.setValue(7, "1");
        ins.setValue(8, "0");
        ins.setValue(9, "0");
        ins.setValue(10, "15");
        ins.setValue(11, "f");
        ins.setValue(12, "Others");
        ins.setValue(13, "yes");
        ins.setValue(14, "yes");
        ins.setValue(15, "Europe");
        ins.setValue(16, "yes");
        ins.setValue(17, "?");
        ins.setValue(18, "4-11 years");
        ins.setValue(19, "Parent");
*/


/*        Class que pode reencrever uma nova saida de Arff
          ArffSaver saver = new ArffSaver();
          saver.setInstances(dataSet);
          saver.setFile(new File(NewFileNamePath));
          saver.writeBatch();
*/

      // Evaluate classifier with test dataset
        ClusterEvaluation evaluation = null;
        try{
            evaluation = new ClusterEvaluation();
            evaluation.evaluateClusterer(testdataset);
            evaluation.clusterResultsToString();
        }catch (Exception ex) {
            Logger.getLogger(ModelGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }


        String evalsummary = Evaluation(ann, traindataset, testdataset);
        System.out.println("Evaluation: " + evalsummary);

    }

}
